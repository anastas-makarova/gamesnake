// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NEWSNAKE_NewSnakeGameModeBase_generated_h
#error "NewSnakeGameModeBase.generated.h already included, missing '#pragma once' in NewSnakeGameModeBase.h"
#endif
#define NEWSNAKE_NewSnakeGameModeBase_generated_h

#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_SPARSE_DATA
#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_RPC_WRAPPERS
#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANewSnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ANewSnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ANewSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NewSnake"), NO_API) \
	DECLARE_SERIALIZER(ANewSnakeGameModeBase)


#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesANewSnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ANewSnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ANewSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NewSnake"), NO_API) \
	DECLARE_SERIALIZER(ANewSnakeGameModeBase)


#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANewSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANewSnakeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANewSnakeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANewSnakeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANewSnakeGameModeBase(ANewSnakeGameModeBase&&); \
	NO_API ANewSnakeGameModeBase(const ANewSnakeGameModeBase&); \
public:


#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANewSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANewSnakeGameModeBase(ANewSnakeGameModeBase&&); \
	NO_API ANewSnakeGameModeBase(const ANewSnakeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANewSnakeGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANewSnakeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANewSnakeGameModeBase)


#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_12_PROLOG
#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_SPARSE_DATA \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_RPC_WRAPPERS \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_INCLASS \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_SPARSE_DATA \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	NewSnake_Source_NewSnake_NewSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NEWSNAKE_API UClass* StaticClass<class ANewSnakeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NewSnake_Source_NewSnake_NewSnakeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
