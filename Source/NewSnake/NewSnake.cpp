// Copyright Epic Games, Inc. All Rights Reserved.

#include "NewSnake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NewSnake, "NewSnake" );
