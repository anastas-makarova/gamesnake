// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NewSnakeGameModeBase.generated.h"


UCLASS()
class NEWSNAKE_API ANewSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
